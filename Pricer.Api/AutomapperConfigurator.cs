using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Pricer.Api
{
    public static class AutomapperConfigurator
    {
        public static void AddAutomapper(this IServiceCollection serviceCollection)
        {
            var configuration = new MapperConfiguration(cfg => {
                 
                cfg.AddProfile<Model2ApiProfile>(); 
            });
            configuration.AssertConfigurationIsValid();
            var mapper = new Mapper(configuration);

            serviceCollection.AddSingleton<IMapper>(mapper);
        }
    }
}