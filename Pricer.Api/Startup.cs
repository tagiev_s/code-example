using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using ClientConfig.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Pricer.Core; 
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Middleware;
using Infrastructure.Services;
using Interfaces.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Pricer.Abstracts.Models;
using Pricer.Abstracts.Services;
using Infrastructure.Strategies;
using Bcs.Connectors.Refinitiv;

namespace Pricer.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: "react_admin",
                    builder =>
                    {
                        builder.WithOrigins(Configuration.GetSection("Cors:Origins").Get<string[]>());
                        builder.AllowAnyMethod();
                        builder.AllowAnyHeader();
                        builder.WithExposedHeaders("X-Total-Count");
                        //builder.AllowCredentials();
                    });
            });
             
            services.AddControllers().AddJsonOptions(options => {
                options.JsonSerializerOptions.IgnoreNullValues = true;
                options.JsonSerializerOptions.Converters.Add(new System.Text.Json.Serialization.JsonStringEnumConverter());
            });


            services.AddTransient(sp => new ClientConfigClient(Configuration["Endpoints:ClientConfig"], new HttpClient()));

            services.ConfigureDependencies(Configuration);
            services.ClientConfigClienConfigureDependencies();
            services.AddAutomapper();

            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(x => x.GetCustomAttributes<DisplayNameAttribute>().SingleOrDefault()?.DisplayName ?? x.Name);
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pricer API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });

            

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.Authority = Configuration["Auth:Authority"];
                    x.Audience = Configuration["Auth:Audience"];
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = false,
                        ValidateAudience = true
                    };

                    x.Events = new JwtBearerEvents
                    { 
                        OnAuthenticationFailed = c =>
                        {
                            c.NoResult();

                            c.Response.StatusCode = 500;
                            c.Response.ContentType = "text/plain";

                            return c.Response.WriteAsync(c.Exception.ToString());

                        }
                    };
                });
                
            services.AddHttpContextAccessor();

            services.AddRefinitivConnector(Configuration);

            services.AddInfrastructureServices();

            services.AddSignalR();
            
            services.AddSingleton<QuotesHub>();
            services.AddTransient<ICorrelationIdGeneratorStrategy, CorrelationIdFromHeaderStrategy>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pricer API V1");
            });
             
            app.UseRouting();

            app.UseCors("react_admin");

            app.UseAuthentication();

            app.UseMiddleware<LoggingMiddleware>();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<QuotesHub>("/quotesFeed");
            });

            var storage = serviceProvider.GetRequiredService<IQuoteStorage>();

            var hub = serviceProvider.GetRequiredService<IHubContext<QuotesHub>>();
        }
    }
}
