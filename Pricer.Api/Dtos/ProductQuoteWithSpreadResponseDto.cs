﻿namespace Pricer.Api.Dtos
{
    public class ProductQuoteWithSpreadResponseDto
    {
        public ProductQuoteWithSpreadDto[] Quotes { get; set; } = new ProductQuoteWithSpreadDto[0];
        public ResponseStatus Status { get; set; }

        public string? Error { get; set; }
    }
}