﻿namespace Pricer.Api.Dtos
{
    public class ProductQuoteWithSpreadDto
    {
        public QuoteDto? ProductQuote { get; set; }
        public SpreadDto? Spread { get; set; }

        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
    }
}