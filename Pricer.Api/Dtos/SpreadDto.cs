﻿namespace Pricer.Api.Dtos
{
    public class SpreadDto
    {
        public string SpreadValue { get; set; }
        public string SpreadClass { get; set; }
    }
}