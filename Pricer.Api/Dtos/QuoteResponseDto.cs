﻿using Pricer.Abstracts.Models;

namespace Pricer.Api.Dtos
{
    public class QuoteResponseDto
    {
        public QuoteDto? Quote { get; set; }

        public ResponseStatus Status { get; set; }

        public string? Error { get; set; }
    }
}