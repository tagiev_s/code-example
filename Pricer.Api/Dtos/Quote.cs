﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pricer.Api.Dtos
{
    public class QuoteDto
    {
        public QuoteDto(int clientId, int productId, decimal bid, decimal ask)
        {
            ClientId = clientId;
            ProductId = productId;
            Bid = bid;
            Ask = ask;
        }

        public int ClientId { get; set; }
        public int ProductId { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
        public DateTime QuoteTime { get; set; }
    }
}
