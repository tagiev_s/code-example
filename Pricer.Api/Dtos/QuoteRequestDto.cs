﻿namespace Pricer.Api.Dtos
{
    public class QuoteRequestDto
    {
        public int ClientId { get; set; }
        public int ProductId { get; set; }
    }
}