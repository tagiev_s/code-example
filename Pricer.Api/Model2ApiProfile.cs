﻿using AutoMapper;
using Pricer.Abstracts.Models;
using Pricer.Api.Dtos;

namespace Pricer.Api
{
    public class Model2ApiProfile : Profile
    {
        public Model2ApiProfile()
        {
            CreateMap<ProductQuote, QuoteDto>();
            CreateMap<ProductQuoteSpread, ProductQuoteWithSpreadDto>();
            CreateMap<Spread, SpreadDto>();
        }
    }
}
