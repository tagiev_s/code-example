using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Pricer.Abstracts.Models;

namespace Pricer.Api
{
    public class QuotesHub : Hub
    {
        public async Task SendMessage(Quote quote)
        {
            await Clients.All.SendAsync("ReceiveMessage", quote);
        }
    }
}