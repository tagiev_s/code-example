﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pricer.Abstracts.Services;
using Pricer.Api.Dtos;

namespace Pricer.Api.Controllers
{
    [Authorize(Roles = Roles.User)]
    [ApiController]
    [Route("[controller]")]
    public class QuoteWithSpreadController : ControllerBase
    {
        private readonly IQuoteService _quoteService;
        private readonly IMapper _mapper;

        public QuoteWithSpreadController(IQuoteService quoteService, IMapper mapper)
        {
            _quoteService = quoteService;
            _mapper = mapper;
        } 

        [HttpGet]
        public async Task<ProductQuoteWithSpreadResponseDto> GetAll()
        {
            try
            {
                var quote = await _quoteService.GetQuotesWithSpreads();

                var quoteDto = _mapper.Map<ProductQuoteWithSpreadDto[]>(quote);

                return new ProductQuoteWithSpreadResponseDto
                {
                    Quotes = quoteDto,
                    Status = ResponseStatus.Success
                };

            }
            catch (Exception e)
            {
                return new ProductQuoteWithSpreadResponseDto
                {
                    Error = e.Message,
                    Status = ResponseStatus.Error
                };
            }
        }
    }
}