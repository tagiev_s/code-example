﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using Pricer.Abstracts.Services;
using Pricer.Api.Dtos;

namespace Pricer.Api.Controllers
{
    [Authorize(Roles = Roles.User)]
    [ApiController]
    [Route("[controller]")]
    public class QuoteController : ControllerBase
    {
        private readonly IQuoteService _quoteService;
        private readonly IMapper _mapper;

        public QuoteController(IQuoteService quoteService, IMapper mapper)
        {
            _quoteService = quoteService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<QuoteResponseDto> Get([FromQuery]QuoteRequestDto request)
        {
            try
            {
                var quote = await _quoteService.GetQuote(request.ProductId, request.ClientId);

                if(quote == null)
                    throw new Exception("Quote not found");

                var quoteDto = _mapper.Map<QuoteDto>(quote);

                return new QuoteResponseDto
                {
                    Quote = quoteDto,
                    Status = ResponseStatus.Success
                };

            }
            catch (Exception e)
            {
                return new QuoteResponseDto
                {
                    Error = e.Message,
                    Status = ResponseStatus.Error
                };
            }
        } 
    }
}
