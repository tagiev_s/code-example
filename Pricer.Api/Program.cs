using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace Pricer.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var configuration = ConifgurationExtensions.BuildConfigurationRoot();

            var elasticUri = new Uri(configuration["ElasticConfiguration:Uri"]);

            var generalIndexFormat = configuration["ElasticConfiguration:IndexFormats:General"];

            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
                .UseSerilog(
                    (context, c) =>
                        c.WriteTo.Console(LogEventLevel.Warning)
                            .WriteTo.Elasticsearch(
                                new ElasticsearchSinkOptions(elasticUri)
                                {
                                    AutoRegisterTemplate = true,
                                    IndexFormat = generalIndexFormat,
                                    MinimumLogEventLevel = LogEventLevel.Warning
                                }).MinimumLevel.Debug());
        }
    }
}
