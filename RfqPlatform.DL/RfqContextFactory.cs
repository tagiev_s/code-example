﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RfqPlatform.DL
{
    public class RfqContextFactory : IDesignTimeDbContextFactory<RfqContext>
    {
        public RfqContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<RfqContext>();
            optionsBuilder.UseSqlServer("");
            return new RfqContext(optionsBuilder.Options);
        }
    }
}