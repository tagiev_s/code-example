﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.DL.Repositories
{
	public class TradeRepository : EFGenericRepository<Dtos.Trade, Abstracts.Models.Trade, string>
	{
		public TradeRepository(RfqContext context, IMapper mapper) : base(context, mapper)
		{
		}
	}
}
