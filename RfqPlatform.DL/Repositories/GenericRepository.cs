﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Abstracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.DL.Repositories
{
	public class EFGenericRepository<TDlEntity, TEntity, TKey> : IGenericRepository<TEntity, TKey>
		where TEntity : IEntity<TKey>
		where TDlEntity : class
	{
		private readonly DbContext _context;
		private readonly IMapper _mapper;
		private readonly DbSet<TDlEntity> _dbSet;

		public EFGenericRepository(DbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
			_dbSet = context.Set<TDlEntity>();
		}

		public async Task<IEnumerable<TEntity>> Get()
		{
			return (await _dbSet.AsNoTracking().ToListAsync()).Select(x => _mapper.Map<TEntity>(x));
		}
		public async Task<TEntity> FindById(TKey id)
		{
			return _mapper.Map<TEntity>(await _dbSet.FindAsync(id));
		}

		public async Task<TEntity> Create(TEntity item)
		{
			var res = _mapper.Map<TDlEntity>(item);
			_dbSet.Add(res);
			await _context.SaveChangesAsync();
			return _mapper.Map<TEntity>(res);
		}
		public async Task Update(TEntity item)
		{
			_context.Entry(_mapper.Map<TDlEntity>(item)).State = EntityState.Modified;
			await _context.SaveChangesAsync();
		}
		public async Task Remove(TEntity item)
		{
			_dbSet.Remove(_mapper.Map<TDlEntity>(item));
			await _context.SaveChangesAsync();
		}
	}
}
