﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.DL.Repositories
{
	public class QuoteRepository : EFGenericRepository<Dtos.Quote, Abstracts.Models.Quote, string>
	{
		public QuoteRepository(RfqContext context, IMapper mapper) : base(context, mapper)
		{
		}
	}
}
