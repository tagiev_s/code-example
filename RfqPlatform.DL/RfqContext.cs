﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace RfqPlatform.DL
{
    public class RfqContext : DbContext
    { 

        public RfqContext([NotNull] DbContextOptions options) : base(options)
        { 
        }

        public virtual DbSet<Dtos.Quote> Quotes { get; set; }
        public virtual DbSet<Dtos.Trade> Trades { get; set; }
    }
}
