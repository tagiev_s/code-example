﻿using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.DL.Dtos
{
	public class Trade
	{
		[Required]
		public string Id { get; set; } = null!;
		[Required]
		public string QuoteId { get; set; } = null!;
		public int ClientId { get; set; }
		[Required]
		public string ClientRequestId { get; set; } = null!;
		public DateTimeOffset TradeTime { get; set; }
		public DateTimeOffset ConfirmationTime { get; set; }
		public decimal TradeVolBase { get; set; }
		public decimal TradeVolValuation { get; set; }
		public TradeDirection SideForBaseCurrency { get; set; }
		public decimal TradePrice { get; set; }
		public TradeStatus TradeStatus { get; set; }
		public string? RejectReason { get; set; }
		public DateTime? SettlementDate { get; set; }
		public decimal DealerFeeValue { get; set; }
	}
}
