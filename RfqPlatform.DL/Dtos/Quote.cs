﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.DL.Dtos
{   
    public class Quote
    { 
        public string Id { get; set; } = null!;
        public int ProductId { get; set; }
        public int ClientId { get; set; }
        [Required]
        public string ClientRequestId { get; set; } = null!;
        public DateTimeOffset QuoteTime { get; set; }
        public int Lifetime { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
        public decimal DealerFee { get; set; }
    }
}
