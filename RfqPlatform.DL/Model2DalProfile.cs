﻿using AutoMapper;
using Pricer.Client;
using RfqPlatform.Abstracts.Models;

namespace RfqPlatform.DL
{
    public class Model2DalProfile : Profile
    {
        public Model2DalProfile()
        {
            CreateMap<Abstracts.Models.Quote, Dtos.Quote>();
            CreateMap<Dtos.Quote, Abstracts.Models.Quote>();

            CreateMap<Dtos.Trade, Abstracts.Models.Trade>();
            CreateMap<Abstracts.Models.Trade, Dtos.Trade>();
        }
    }
}