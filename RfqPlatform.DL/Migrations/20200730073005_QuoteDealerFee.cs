﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RfqPlatform.DL.Migrations
{
    public partial class QuoteDealerFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DealerFee",
                table: "Quotes",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealerFee",
                table: "Quotes");
        }
    }
}
