﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RfqPlatform.DL.Migrations
{
    public partial class QuoteModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Quotes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    ClientRequestId = table.Column<string>(nullable: false),
                    QuoteTime = table.Column<DateTimeOffset>(nullable: false),
                    Lifetime = table.Column<int>(nullable: false),
                    Bid = table.Column<decimal>(nullable: false),
                    Ask = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quotes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Quotes");
        }
    }
}
