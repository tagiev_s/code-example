﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RfqPlatform.DL.Migrations
{
    public partial class TradeModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Trades",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    QuoteId = table.Column<string>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    ClientRequestId = table.Column<string>(nullable: false),
                    TradeTime = table.Column<DateTimeOffset>(nullable: false),
                    ConfirmationTime = table.Column<DateTimeOffset>(nullable: false),
                    TradeVolBase = table.Column<decimal>(nullable: false),
                    TradeVolValuation = table.Column<decimal>(nullable: false),
                    SideForBaseCurrency = table.Column<int>(nullable: false),
                    TradePrice = table.Column<decimal>(nullable: false),
                    TradeStatus = table.Column<int>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true),
                    SettlementDate = table.Column<DateTime>(nullable: true),
                    DealerFeeValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trades", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Trades");
        }
    }
}
