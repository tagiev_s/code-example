﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pricer.Client;
using RfqPlatform.Abstracts.Repositories;
using RfqPlatform.DL.Repositories;

namespace RfqPlatform.DL
{
    public static class DependencyConfiguration
    {
        public static void AddDal(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddDbContext<RfqContext>(options => options.UseSqlServer(configuration.GetConnectionString("CFT")), ServiceLifetime.Transient);
            serviceCollection.AddTransient<IGenericRepository<Abstracts.Models.Quote, string>, QuoteRepository>();
            serviceCollection.AddTransient<IGenericRepository<Abstracts.Models.Trade, string>, TradeRepository>();
        }
    }
}
