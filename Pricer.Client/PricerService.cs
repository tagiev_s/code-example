﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pricer.Client
{
	public class PricerService : IPricerService
	{
		private readonly PricerClient _client;

		public PricerService(PricerClient client)
		{
			_client = client ?? throw new ArgumentNullException(nameof(client));
		}

		public async Task<QuoteDto> QuoteAsync(int? clientId, int? productId)
		{
			var res = await _client.QuoteAsync(clientId, productId);
			if(res.Status == ResponseStatus.Error)
			{
				throw new Exception($"Price service error: {res.Error}");
			}
			return res.Quote;
		}
	}
}
