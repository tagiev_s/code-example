﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace Pricer.Client
{
    public partial class PricerClient
    {
        public string Token { get; set; }

        public string CorrelationId { get; set; }
        
        partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request,
            string url)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            request.Headers.Add("CorrelationId", CorrelationId);
        }

        public PricerClient(string baseUrl, System.Net.Http.HttpClient httpClient, string token, string correlationId)
             :this(baseUrl, httpClient)
        {
            Token = token;
            CorrelationId = correlationId;
        }
    }
}
