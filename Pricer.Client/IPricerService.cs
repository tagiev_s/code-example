﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pricer.Client
{
	public interface IPricerService
	{
		Task<QuoteDto> QuoteAsync(int? clientId, int? productId);
	}
}
