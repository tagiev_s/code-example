﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pricer.Client
{
	public static class PricerClientDependencyConfiguration
	{
		public static void PricerClienConfigureDependencies(this IServiceCollection serviceCollection)
		{
			serviceCollection.AddTransient<IPricerService, PricerService>();
		}
	}
}
