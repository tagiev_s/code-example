﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Abstracts.Handlers;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Abstracts.Repositories;
using RfqPlatform.Core.Exceptions;

namespace RfqPlatform.Core.Handlers
{
	public class TradeCreateCommandHandler: ICommandHandler<TradeCreateCommand, Trade>
	{
		private readonly IGenericRepository<Trade, string> _tradeRepository;
		private readonly IGenericRepository<Quote, string> _quoteRepository;

		public TradeCreateCommandHandler(IGenericRepository<Trade, string> tradeRepository, IGenericRepository<Quote, string> quoteRepository)
		{
			_tradeRepository = tradeRepository;
			_quoteRepository = quoteRepository;
		}

		public async Task<Trade> Handle(TradeCreateCommand command)
		{
			var quote = await _quoteRepository.FindById(command.QuoteId);
			if(quote == null)
			{
				throw new ValidationException($"{nameof(TradeCreateCommand)}.{nameof(TradeCreateCommand.QuoteId)}",
					$"Can't find quote by id = {command.QuoteId}");
			}

			var price = command.SideForBaseCurrency == TradeDirection.Buy ? quote.Ask : quote.Bid;
			var tradeVolValuation = command.TradeVolBase * price;

			if(Math.Abs(command.TradeVolValuation - tradeVolValuation) > 0.01M)
			{
				throw new ValidationException($"{nameof(TradeCreateCommand)}.{nameof(TradeCreateCommand.TradeVolValuation)}",
					$"{nameof(TradeCreateCommand.TradeVolValuation)} is not expected. It must be {tradeVolValuation} (price = {price} for {command.SideForBaseCurrency.ToString()} * {nameof(TradeCreateCommand.TradeVolBase)} = {command.TradeVolBase}). ");
			}

			var trade = new Trade(
				command.QuoteId,
				quote.ClientId,
				command.ClientRequestId,
				command.TradeTime,
				DateTimeOffset.Now,
				command.TradeVolBase,
				tradeVolValuation,
				command.SideForBaseCurrency,
				price,
				TradeStatus.Accepted,
				quote.DealerFee
			);

			return await _tradeRepository.Create(trade);
			
		}
	}
}
