﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using ClientConfig.Client;
using Pricer.Client;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Abstracts.Handlers;
using RfqPlatform.Abstracts.Managers;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Abstracts.Repositories;

namespace RfqPlatform.Core.Handlers
{
	public class QuoteCreateCommandHandler : ICommandHandler<QuoteRequestCommand, Quote>
	{
		private readonly IPricerService _pricerService;
		private readonly IGenericRepository<Quote, string> _quoteRepository;
		private readonly IClientManager _clientManager;
		private readonly IClientConfigService _clientConfigService;

		public QuoteCreateCommandHandler(IPricerService pricerService,
								   IGenericRepository<Quote, string> quoteRepository,
								   IClientManager clientManager,
								   IClientConfigService clientConfigService)
		{
			_pricerService = pricerService;
			_quoteRepository = quoteRepository;
			_clientManager = clientManager;
			_clientConfigService = clientConfigService;
		}

		public async Task<Quote> Handle(QuoteRequestCommand command)
		{
			var currentClient = await _clientManager.GetCurrent();
			var quote = await _pricerService.QuoteAsync(currentClient.Id, int.Parse(command.ProductId));
			var spread = await _clientConfigService.GetClientSpread(currentClient.Id, int.Parse(command.ProductId));

			var newQuoteDto = new Quote(
				quote.ProductId,
				quote.ClientId,
				command.ClientRequestId, 
				quote.QuoteTime,
				120,
				(decimal)quote.Bid,
				(decimal)quote.Ask,
				(decimal)spread.SpreadValue
			);


			return await _quoteRepository.Create(newQuoteDto);
		}
	}
}
