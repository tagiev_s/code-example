﻿using AutoMapper;
using ClientConfig.Client;
using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using CaseExtensions;

namespace RfqPlatform.Core.MapperProfilies
{
	public class ApiToModelProfile: Profile
	{
		public ApiToModelProfile()
		{
			CreateMap<ProductCardDto, Product>()
				.ForCtorParam(nameof(Product.BaseCurrency).ToCamelCase(), opt => opt.MapFrom(src => src.Product.BaseCurrency))
				.ForCtorParam(nameof(Product.BaseLotSize).ToCamelCase(), opt => opt.MapFrom(src => 1))
				.ForCtorParam(nameof(Product.MaxLimitPerQuote).ToCamelCase(), opt => opt.MapFrom(src => (int?)src.BaseLimit ?? int.MaxValue))
				.ForCtorParam(nameof(Product.Id).ToCamelCase(), opt => opt.MapFrom(src => src.Product.Id))
				.ForCtorParam(nameof(Product.SettlementType).ToCamelCase(), opt => opt.MapFrom(src => src.Product.SettlementType))
				.ForCtorParam(nameof(Product.ValuationCurrency).ToCamelCase(), opt => opt.MapFrom(src => src.Product.ValuationCurrency))
				.ForAllMembers(x => x.Ignore());

			CreateMap<ClientDto, Client>();
		}
	}
}
