﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pricer.Client;
using RfqPlatform.Abstracts.Accessors;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Abstracts.Handlers;
using RfqPlatform.Abstracts.Managers;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Core.Accessors;
using RfqPlatform.Core.Handlers;
using RfqPlatform.Core.Managers;

namespace RfqPlatform.Core
{
	public static class DependencyConfiguration
	{
		public static void AddCore(this IServiceCollection serviceCollection, IConfiguration configuration)
		{
			serviceCollection.AddTransient<ICommandHandler<QuoteRequestCommand, Quote>, QuoteCreateCommandHandler>();
			serviceCollection.AddTransient<ICommandHandler<TradeCreateCommand, Trade>, TradeCreateCommandHandler>();
			serviceCollection.AddTransient<IProductAccessor, ProductAccessor>();
			serviceCollection.AddTransient<IClientAccessor, ClientAccessor>();
			serviceCollection.AddTransient<IClientManager, ClientManager>();
		}
	}
}
