﻿using AutoMapper;
using ClientConfig.Client;
using Microsoft.Extensions.Caching.Memory;
using RfqPlatform.Abstracts.Accessors;
using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Core.Accessors
{
	public class ClientAccessor : IClientAccessor
	{
		private readonly IClientConfigService _clientConfigService;
		private readonly IMapper _mapper;
		private readonly IMemoryCache _cache;
		private static object CacheKey = new object();

		public ClientAccessor(IClientConfigService clientConfigService, IMapper mapper, IMemoryCache cache)
		{
			_clientConfigService = clientConfigService;
			_mapper = mapper;
			_cache = cache;
		}
		public async Task<Client> GetByName(string name)
		{
			var all = await GetAll();
			return all.FirstOrDefault(x => x.Name == name);
		}

		private async Task<IReadOnlyCollection<Client>> GetAll()
		{
			return await _cache.GetOrCreateAsync(CacheKey, async (entry) =>
			{
				entry.SetAbsoluteExpiration(System.TimeSpan.FromMinutes(5));
				var clients = await _clientConfigService.GetAllClients();
				return clients.Select(c => _mapper.Map<Client>(c)).ToList();
			});
		}
	}
}
