﻿using AutoMapper;
using ClientConfig.Client;
using RfqPlatform.Abstracts.Accessors;
using RfqPlatform.Abstracts.Managers;
using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Core.Accessors
{
	public class ProductAccessor : IProductAccessor
	{
		private readonly IClientConfigService _clientConfigService;
		private readonly IClientManager _clientManager;
		private readonly IMapper _mapper;

		public ProductAccessor(IClientConfigService clientConfigService, IClientManager clientManager, IMapper mapper)
		{
			_clientConfigService = clientConfigService;
			_clientManager = clientManager;
			_mapper = mapper;
		}
		public async Task<IReadOnlyCollection<Product>> GetAll()
		{
			var currentClient = await _clientManager.GetCurrent();
			var dtos = await _clientConfigService.GetClientProductCards(currentClient.Name);
			return dtos.Select(x => _mapper.Map<Product>(x)).ToList();
		}
	}
}
