﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Core.Exceptions
{
	public class ValidationException: Exception
	{
		public string Field { get; set; }
		public ValidationException(string field, string message)
			:base()
		{
			Field = field;
		}
	}
}
