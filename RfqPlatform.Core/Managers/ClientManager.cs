﻿using Microsoft.AspNetCore.Http;
using RfqPlatform.Abstracts.Accessors;
using RfqPlatform.Abstracts.Managers;
using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Core.Managers
{
	public class ClientManager : IClientManager
	{
		private readonly IClientAccessor _clientAccessor;
		private readonly IHttpContextAccessor _httpContextAccessor;

		public ClientManager(IClientAccessor clientAccessor, IHttpContextAccessor httpContextAccessor)
		{
			_clientAccessor = clientAccessor;
			_httpContextAccessor = httpContextAccessor;
		}
		public Task<Client> GetCurrent()
		{
			var claims = _httpContextAccessor.HttpContext.User.Claims.ToArray();
			var name = claims.FirstOrDefault(x => x.Type == "preferred_username");

			return _clientAccessor.GetByName(name.Value);
		}
	}
}
