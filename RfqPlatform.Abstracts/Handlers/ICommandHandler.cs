﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Abstracts.Handlers
{
	public interface ICommandHandler<TCommand>
	{
		Task Handle(TCommand command);
	}
	public interface ICommandHandler<TCommand, TCommandResponse>
	{
		Task<TCommandResponse> Handle(TCommand command);
	}
}
