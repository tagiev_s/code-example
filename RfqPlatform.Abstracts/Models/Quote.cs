﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public class Quote: IEntity<string>
	{
		public Quote(int productId, int clientId, string clientRequestId, DateTimeOffset quoteTime, int lifetime, decimal bid, decimal ask, decimal dealerFee)
		{
			Id = CreateId(productId, clientId, quoteTime);
			ProductId = productId;
			ClientId = clientId;
			ClientRequestId = clientRequestId ?? throw new ArgumentNullException(nameof(clientRequestId));
			QuoteTime = quoteTime;
			Lifetime = lifetime;
			Bid = bid;
			Ask = ask;
			DealerFee = dealerFee;
		}

		public string Id { get; set; }
		public int ProductId { get; set; }
		public int ClientId { get; set; }
		public string ClientRequestId { get; set; }
		public DateTimeOffset QuoteTime { get; set; }
		public int Lifetime { get; set; }
		public decimal Bid { get; set; }
		public decimal Ask { get; set; }
		public decimal DealerFee { get; set; }

		private static string CreateId(int productId, int clientId, DateTimeOffset quoteTime)
		{
			return $"{clientId}_{productId}_{quoteTime.Ticks}";
		}
	}
}
