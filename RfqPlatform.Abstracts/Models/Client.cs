﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public class Client
	{
		public Client(string name)
		{
			Name = name ?? throw new ArgumentNullException(nameof(name));
		}

		public int Id { get; set; }
		public string Name { get; set; }
	}
}
