﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public class Trade : IEntity<string>
	{
		public Trade(string quoteId, int clientId, string clientRequestId, DateTimeOffset tradeTime, DateTimeOffset confirmationTime, decimal tradeVolBase, decimal tradeVolValuation, TradeDirection sideForBaseCurrency, decimal tradePrice, TradeStatus tradeStatus, decimal dealerFeeValue)
		{
			Id = CreateId(quoteId, confirmationTime);
			QuoteId = quoteId ?? throw new ArgumentNullException(nameof(quoteId));
			ClientId = clientId;
			ClientRequestId = clientRequestId;
			TradeTime = tradeTime;
			ConfirmationTime = confirmationTime;
			TradeVolBase = tradeVolBase;
			TradeVolValuation = tradeVolValuation;
			SideForBaseCurrency = sideForBaseCurrency;
			TradePrice = tradePrice;
			TradeStatus = tradeStatus;
			DealerFeeValue = dealerFeeValue;
		}

		public string Id { get; set; }
		public string QuoteId { get; set; }
		public int ClientId { get; set; }
		public string ClientRequestId { get; set; }
		public DateTimeOffset TradeTime { get; set; }
		public DateTimeOffset ConfirmationTime { get; set; }
		public decimal TradeVolBase { get; set; }
		public decimal TradeVolValuation { get; set; }
		public TradeDirection SideForBaseCurrency { get; set; }
		public decimal TradePrice { get; set; }
		public TradeStatus TradeStatus { get; set; }
		public string? RejectReason { get; set; }
		public DateTime? SettlementDate { get; set; }
		public decimal DealerFeeValue { get; set; }

		private static string CreateId(string quoteId, DateTimeOffset ConfirmationTime)
		{
			return $"{quoteId}-{ConfirmationTime.Ticks}";
		}
	}
}
