﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public class Currency
	{
		public Currency(string currencyCode)
		{
			CurrencyCode = currencyCode ?? throw new ArgumentNullException(nameof(currencyCode));
		}

		public string CurrencyCode { get; set; }
	}
}
