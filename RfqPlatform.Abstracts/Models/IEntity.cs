﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public interface IEntity<TKey>
	{
		public TKey Id { get; set; }
	}
}
