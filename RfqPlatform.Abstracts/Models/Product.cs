﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Models
{
	public class Product
	{
		public Product(int id, string baseCurrency, string valuationCurrency, string settlementType, int maxLimitPerQuote, int baseLotSize)
		{
			Id = id;
			BaseCurrency = baseCurrency ?? throw new ArgumentNullException(nameof(baseCurrency));
			ValuationCurrency = valuationCurrency ?? throw new ArgumentNullException(nameof(valuationCurrency));
			SettlementType = settlementType ?? throw new ArgumentNullException(nameof(settlementType));
			MaxLimitPerQuote = maxLimitPerQuote;
			BaseLotSize = baseLotSize;
		}

		public int Id { get; set; }
		public string BaseCurrency { get; set; }
		public string ValuationCurrency { get; set; }
		public string SettlementType { get; set; }
		public int MaxLimitPerQuote { get; set; }
		public int BaseLotSize { get; set; }
	}
}
