﻿using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Abstracts.Accessors
{
	public interface IClientAccessor
	{
		Task<Client> GetByName(string name);
	}
}
