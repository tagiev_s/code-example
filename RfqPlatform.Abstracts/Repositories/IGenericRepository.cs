﻿using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Abstracts.Repositories
{
	public interface IGenericRepository<TEntity, TKey> where TEntity : IEntity<TKey>
	{
		Task<TEntity> Create(TEntity item);
		Task<TEntity> FindById(TKey id);
		Task<IEnumerable<TEntity>> Get();
		Task Remove(TEntity item);
		Task Update(TEntity item);
	}
}
