﻿using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Commands
{
	public class TradeCreateCommand: BaseCommand
	{
		public TradeCreateCommand(string quoteId, DateTimeOffset tradeTime, decimal tradeVolBase, decimal tradeVolValuation, TradeDirection sideForBaseCurrency, string clientRequestId)
			:base(clientRequestId)
		{
			QuoteId = quoteId ?? throw new ArgumentNullException(nameof(quoteId));
			TradeTime = tradeTime;
			TradeVolBase = tradeVolBase;
			TradeVolValuation = tradeVolValuation;
			SideForBaseCurrency = sideForBaseCurrency;
		}

		public string QuoteId { get; set; }
		public DateTimeOffset TradeTime { get; set; }
		public decimal TradeVolBase { get; set; }
		public decimal TradeVolValuation { get; set; }
		public TradeDirection SideForBaseCurrency { get; set; }
	}
}
