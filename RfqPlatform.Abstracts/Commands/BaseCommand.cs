﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Commands
{
	public abstract class BaseCommand
	{
		public BaseCommand(string clientRequestId)
		{
			ClientRequestId = clientRequestId ?? throw new ArgumentNullException(nameof(clientRequestId));
		}

		public string ClientRequestId { get; set; }
	}
}
