﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Abstracts.Commands
{
	public class QuoteRequestCommand: BaseCommand
	{
		public QuoteRequestCommand(string productId, string clientRequestId)
			:base(clientRequestId)
		{
			ProductId = productId ?? throw new ArgumentNullException(nameof(productId));
		}

		public string ProductId { get; set; }
		
	}
}
