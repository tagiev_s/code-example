﻿using RfqPlatform.Abstracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RfqPlatform.Abstracts.Managers
{
	public interface IClientManager
	{
		Task<Client> GetCurrent();
	}
}
