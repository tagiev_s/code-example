Здесь представлены выдержки из кода

# Основные моменты
- Код разделён на слои (абстракция, бизнес логика, доступ к данным, презентация)
- Есть 2 микросервиса RFQPlatform и Pricer
- У микросервиса Pricer есть библиотека Pricer.Client, где инкапсулирована логика вызова микросервиса (Код сгенерирован, через NSwag)
- Для последующего трекинга запросов используется паттерн CorrelationId
- Запросы отделены от команд ([Accessors] и [Handlers])
- Используется дженерик [репозиторий][generic]

[accessors]: https://bitbucket.org/tagiev_s/code-example/src/master/RfqPlatform.Core/Accessors/
[handlers]: https://bitbucket.org/tagiev_s/code-example/src/master/RfqPlatform.Core/Handlers/
[generic]: https://bitbucket.org/tagiev_s/code-example/src/master/RfqPlatform.DL/Repositories/GenericRepository.cs