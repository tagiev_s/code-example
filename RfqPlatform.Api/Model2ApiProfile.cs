﻿using AutoMapper;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Api.Dtos.Models;

namespace RfqPlatform.Api
{
    public class Model2ApiProfile : Profile
    {
        public Model2ApiProfile()
        {
            CreateMap<Quote, QuoteDto>();
            CreateMap<Product, ProductDto>();
            CreateMap<Trade, TradeDto>();
        }
    }
}