using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using RfqPlatform.Core.MapperProfilies;
using RfqPlatform.DL;

namespace RfqPlatform.Api
{
    public static class AutomapperConfigurator
    {
        public static void AddAutomapper(this IServiceCollection serviceCollection)
        {
            var configuration = new MapperConfiguration(cfg => {

                cfg.AddProfile<Model2DalProfile>();
                cfg.AddProfile<Api2ModelProfile>();
                cfg.AddProfile<Model2ApiProfile>();
                cfg.AddProfile<ApiToModelProfile>();
            });
            configuration.AssertConfigurationIsValid();
            var mapper = new Mapper(configuration);

            serviceCollection.AddSingleton<IMapper>(mapper);
        }
    }
}