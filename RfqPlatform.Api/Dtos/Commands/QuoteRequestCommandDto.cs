﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.Api.Dtos.Commands
{
	[DisplayName("QuoteRequest")]
	public class QuoteRequestCommandDto: BaseCommandDto
	{
		/// <summary>
		/// Идентификатор продукта
		/// </summary>
		[Required]
		public string ProductId { get; set; } = null!;
	}
}
