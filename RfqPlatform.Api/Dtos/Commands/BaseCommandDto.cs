﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Dtos.Commands
{
	public abstract class BaseCommandDto
	{
		/// <summary>
		/// Уникальный идентификатор запроса
		/// </summary>
		[Required]
		public string ClientRequestId { get; set; } = null!;
	}
}
