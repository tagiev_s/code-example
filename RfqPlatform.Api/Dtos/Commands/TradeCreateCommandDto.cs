﻿using RfqPlatform.Abstracts.Models;
using RfqPlatform.Api.Dtos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.Api.Dtos.Commands
{
	[DisplayName("TradeCreate")]
	public class TradeCreateCommandDto: BaseCommandDto
	{
		/// <summary>
		/// Идентификатор квоты, по которой идет сделка
		/// </summary>
		[Required]
		public string QuoteId { get; set; } = null!;
		/// <summary>
		/// Время генерации заявки клиентом
		/// </summary>
		[Required]
		public DateTimeOffset TradeTime { get; set; }
		/// <summary>
		/// Сумма сделки в базовой валюте. 2 знака после точки
		/// </summary>
		[Required]
		public decimal TradeVolBase { get; set; }
		/// <summary>
		/// Сумма сделки в валюте котировки. 2 знака после точки. Для сверки
		/// </summary>
		[Required]
		public decimal TradeVolValuation { get; set; }
		/// <summary>
		/// Сторона сделки клиента относительно БКС
		/// </summary>
		[Required]
		public TradeDirectionEnum SideForBaseCurrency { get; set; }
	}
}
