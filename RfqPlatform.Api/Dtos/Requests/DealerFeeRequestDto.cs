﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Dtos.Requests
{
	[DisplayName("DealerFeeRequest")]
	public class DealerFeeRequestDto
	{
		/// <summary>
		/// Идентификатор сделки
		/// </summary>
		[Required]
		public string TradeId { get; set; } = null!;
	}
}
