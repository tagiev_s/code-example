﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Dtos.Requests
{
	[DisplayName("DealerFeeResponse")]
	public class DealerFeeResponseDto
	{
		/// <summary>
		/// Идентификатор сделки
		/// </summary>
		[Required]
		public string TradeId { get; set; } = null!;

		/// <summary>
		/// Размер комиссии Дилера по конкретной сделке. В рублях. 4 знака после точки
		/// </summary>
		[Required]
		public decimal DealerFeeValue { get; set; }
	}
}
