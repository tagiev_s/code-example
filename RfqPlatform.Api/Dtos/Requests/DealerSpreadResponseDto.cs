﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Dtos.Requests
{
	[DisplayName("DealerSpreadResponse")]
	public class DealerSpreadResponseDto
	{
		/// <summary>
		/// Идентификатор конечного Банка-клиента
		/// </summary>
		[Required]
		public string ClientId { get; set; } = null!;

		/// <summary>
		/// Идентификатор продукта
		/// </summary>
		[Required]
		public string ProductId { get; set; } = null!;

		/// <summary>
		/// Спред Дилера применяемый к данному продукту. 4 знака после точки
		/// </summary>
		[Required]
		public decimal DealerSpreadValue { get; set; }
	}
}
