﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Api.Dtos.Models
{
	public enum TradeStatusEnum
	{
		Accepted, Rejected
	}
}
