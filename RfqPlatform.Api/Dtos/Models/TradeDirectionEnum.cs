﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RfqPlatform.Api.Dtos.Models
{
	/// <summary>
	/// Сторона сделки клиента относительно БКС
	/// </summary>
	public enum TradeDirectionEnum
	{
		Buy, Sell
	}
}
