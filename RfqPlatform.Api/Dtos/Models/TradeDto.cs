﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.Api.Dtos.Models
{
	[DisplayName("Trade")]
	public class TradeDto
	{
		[Required]
		public string Id { get; set; } = null!;
		/// <summary>
		/// Идентификатор котировки
		/// </summary>
		[Required]
		public string QuoteId { get; set; } = null!;

		/// <summary>
		/// Идентификатор конечного Банка-клиента 
		/// </summary>
		[Required]
		public int ClientId { get; set; }

		/// <summary>
		/// Уникальный идентификатор запроса
		/// </summary>
		[Required]
		public string ClientRequestId { get; set; } = null!;

		/// <summary>
		/// Время генерации заявки клиентом
		/// </summary>
		[Required]
		public DateTimeOffset TradeTime { get; set; }

		/// <summary>
		/// Время подтверждения сделки
		/// </summary>
		[Required]
		public DateTimeOffset ConfirmationTime { get; set; }

		/// <summary>
		/// Сумма сделки в базовой валюте. 2 знака после точки
		/// </summary>
		[Required]
		public decimal TradeVolBase { get; set; }

		/// <summary>
		/// Сумма сделки в валюте котировки. 2 знака после точки
		/// </summary>
		[Required]
		public decimal TradeVolValuation { get; set; }

		/// <summary>
		/// Сторона сделки клиента относительно БКС
		/// </summary>
		[Required]
		public TradeDirectionEnum SideForBaseCurrency { get; set; }

		/// <summary>
		/// Цена сделки
		/// </summary>
		[Required]
		public decimal TradePrice { get; set; }

		/// <summary>
		/// Подтверждена, либо отклонена
		/// </summary>
		[Required]
		public TradeStatusEnum TradeStatus { get; set; }

		/// <summary>
		/// Причина отказа
		/// </summary>
		public string? RejectReason { get; set; }

		/// <summary>
		/// Дата исполнения сделки
		/// </summary>
		public DateTime? SettlementDate { get; set; }
	}
}
