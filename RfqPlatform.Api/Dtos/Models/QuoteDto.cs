﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.Api.Dtos.Models
{
	[DisplayName("Quote")]
	public class QuoteDto
	{
		[Required]
		public string Id { get; set; } = null!;
		/// <summary>
		/// Идентификатор продукта
		/// </summary>
		[Required]
		public string ProductId { get; set; } = null!;
		/// <summary>
		/// Идентификатор конечного Банка-клиента, для которого сгенерирована котировка
		/// </summary>
		[Required]
		public string ClientId { get; set; } = null!;
		/// <summary>
		/// Уникальный идентификатор запроса
		/// </summary>
		[Required]
		public string ClientRequestId { get; set; } = null!;
		/// <summary>
		/// Время генерации котировки
		/// </summary>
		[Required]
		public DateTimeOffset QuoteTime { get; set; }
		/// <summary>
		/// Время жизни котировки, в секундах
		/// </summary>
		[Required]
		public int Lifetime { get; set; }
		/// <summary>
		/// Цена покупки БКС. 4 знака после точки
		/// </summary>
		[Required]
		public decimal Bid { get; set; }
		/// <summary>
		/// Цена продажи БКС. 4 знака после точки
		/// </summary>
		[Required]
		public decimal Ask { get; set; }
	}
}
