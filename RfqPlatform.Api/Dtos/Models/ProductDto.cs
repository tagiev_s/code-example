﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RfqPlatform.Api.Dtos.Models
{
	[DisplayName("Product")]
	public class ProductDto
	{
		/// <summary>
		/// Идентификатор продукта
		/// </summary>
		[Required]
		public string Id { get; set; } = null!;
		/// <summary>
		/// Базовая валюта
		/// </summary>
		[Required]
		public string BaseCurrency { get; set; } = null!;
		/// <summary>
		/// Валюта котировки
		/// </summary>
		[Required]
		public string ValuationCurrency { get; set; } = null!;
		/// <summary>
		/// Тип исполнения. T + SettlementType
		/// </summary>
		[Required]
		public string SettlementType { get; set; } = null!;
		/// <summary>
		/// Максимальный размер сделки в пределах одной котировки в базовой валюте
		/// </summary>
		[Required]
		public int MaxLimitPerQuote { get; set; }
		/// <summary>
		/// Лотность базовой валюты
		/// </summary>
		[Required]
		public int BaseLotSize { get; set; }
	}
}
