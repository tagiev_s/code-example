﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Dtos
{
	public class ResultDto<T>
		where T:class
	{
		public ResultDto(T payload, ResultStatusEnum status)
		{
			Payload = payload;
			Status = status;
		}

		public ResultDto(string message, string code, ResultStatusEnum status)
		{
			Status = status;
			Message = message;
			Code = code;
		}
		public T? Payload { get; set; }
		public ResultStatusEnum Status { get; set; }

		/// <summary>
		/// Описание ошибки
		/// </summary>
		public string? Message { get; set; }

		/// <summary>
		/// Внутренний код ошибки
		/// </summary>
		public string? Code { get; set; }

		public static ResultDto<U> Ok<U>(U payload)
			where U: class
		{
			return new ResultDto<U>(payload, ResultStatusEnum.Success);
		}

		public static ResultDto<T> Error(string message, string code)
		{
			return new ResultDto<T>(message, code, ResultStatusEnum.Error);
		}
	}
}
