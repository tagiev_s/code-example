﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Abstracts.Handlers;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Api.Dtos;
using RfqPlatform.Api.Dtos.Commands;
using RfqPlatform.Api.Dtos.Models;
using RfqPlatform.Api.SwaggerFilter;
using Swashbuckle.AspNetCore.Annotations;

namespace RfqPlatform.Api.Controllers
{
	[Route("v1/trade")]
	[ApiController]
	[SwaggerTag("Управление сделками")]
	public class TradeController : ControllerBase
	{
		private readonly ICommandHandler<TradeCreateCommand, Trade> _tradeCreateHandler;
		private readonly IMapper _mapper;

		public TradeController(ICommandHandler<TradeCreateCommand, Trade> tradeCreateHandler, IMapper mapper)
		{
			_tradeCreateHandler = tradeCreateHandler;
			_mapper = mapper;
		}

		/// <summary>
		/// Создание сделки
		/// </summary>
		/// <param name="request">Запрос на создание сделки</param>
		/// <response code="200">Сделка или ошибка</response>
		[HttpPost("create")]
		[Consumes(MediaTypeNames.Application.Json)]
		[Produces(MediaTypeNames.Application.Json)]
		[ProducesResponseType(200)]
		[CustomResponseName("TradeResult")]
		public async Task<ResultDto<TradeDto>> TradeCreate(TradeCreateCommandDto request)
		{
			var tradeCreateCommand = _mapper.Map<TradeCreateCommand>(request);
			var trade = await _tradeCreateHandler.Handle(tradeCreateCommand);
			var tradeDto = _mapper.Map<TradeDto>(trade);

			return ResultDto<TradeDto>.Ok(tradeDto);
		}
	}
}