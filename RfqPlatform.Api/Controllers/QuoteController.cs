﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Abstracts.Handlers;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Api.Dtos;
using RfqPlatform.Api.Dtos.Commands;
using RfqPlatform.Api.Dtos.Models;
using RfqPlatform.Api.SwaggerFilter;
using Swashbuckle.AspNetCore.Annotations;

namespace RfqPlatform.Api.Controllers
{
	[Route("v1/quote")]
	[ApiController]
	[SwaggerTag("Управление запросами на котировку")]
	public class QuoteController : ControllerBase
	{
		private readonly ICommandHandler<QuoteRequestCommand, Quote> _quoteRequestHandler;
		private readonly IMapper _mapper;

		public QuoteController(ICommandHandler<QuoteRequestCommand, Quote> quoteRequestHandler, IMapper mapper)
		{
			_quoteRequestHandler = quoteRequestHandler;
			_mapper = mapper;
		}

		/// <summary>
		/// Запрос на котировку
		/// </summary>
		/// <param name="request">Запрос на котировку</param>
		/// <response code="200">Котировка или ошибка</response >
		[HttpPost("request")]
		[Consumes(MediaTypeNames.Application.Json)]
		[Produces(MediaTypeNames.Application.Json)]
		[ProducesResponseType(200)]
		[CustomResponseName("QuoteResult")]
		public async Task<ResultDto<QuoteDto>> QuoteRequest(QuoteRequestCommandDto request)
		{
			try
			{
				var quoteRequestCommand = _mapper.Map<QuoteRequestCommand>(request);
				var quote = await _quoteRequestHandler.Handle(quoteRequestCommand);
				var quoteDto = _mapper.Map<QuoteDto>(quote);

				return ResultDto<QuoteDto>.Ok(quoteDto);
			}
			catch (Exception e)
			{
				return ResultDto<QuoteDto>.Error(e.Message, "quote_request_error");
			}
		}
	}
}