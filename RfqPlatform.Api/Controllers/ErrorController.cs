﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces.Services;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RfqPlatform.Api.Dtos;
using RfqPlatform.Api.Helpers;
using RfqPlatform.Core.Exceptions;

namespace RfqPlatform.Api.Controllers
{
	[ApiController]
	public class ErrorController : ControllerBase
	{
		private readonly ILogger<ResultDto<object>> _log;
		private readonly ICorrelationIdProvider _correlationIdProvider;

		public ErrorController(ILogger<ResultDto<object>> log, ICorrelationIdProvider correlationIdProvider)
		{
			_log = log;
			_correlationIdProvider = correlationIdProvider;
		}

		[Route("/error")]
		[ApiExplorerSettings(IgnoreApi = true)]
		public ResultDto<object> Error([FromServices] IWebHostEnvironment webHostEnvironment)
		{
			var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
			_log.LogCritical("", context.Error);
			ResultDto<object> res = context.Error switch
			{
				ValidationException ex => ResultDto<object>.Error(ex.Message, ErrorCodes.ValidationError),
				_ => ResultDto<object>.Error($"Please contact support: internal error id = {_correlationIdProvider.GetCorrelationId()}", ErrorCodes.UnexpectedError),
			};

			return res;
		}
	}
}