﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RfqPlatform.Abstracts.Accessors;
using RfqPlatform.Abstracts.Models;
using RfqPlatform.Api.Dtos;
using RfqPlatform.Api.Dtos.Models;
using RfqPlatform.Api.SwaggerFilter;
using Swashbuckle.AspNetCore.Annotations;

namespace RfqPlatform.Api.Controllers
{
	[Route("v1/product")]
	[ApiController]
	[SwaggerTag("Получение информации по продуктам")]
	public class ProductController : ControllerBase
	{
		private readonly IProductAccessor _productAccessor;
		private readonly IMapper _mapper;

		public ProductController(IProductAccessor productAccessor, IMapper mapper)
		{
			_productAccessor = productAccessor;
			_mapper = mapper;
		}
		/// <summary>
		/// Получение списка продуктов
		/// </summary>
		/// <response  code="200">Список продуктов</response >
		[HttpGet]
		[Produces(MediaTypeNames.Application.Json)]
		[ProducesResponseType(200)]
		[CustomResponseName("ProductListResult")]
		public async Task<ResultDto<IReadOnlyCollection<ProductDto>>>  Get()
		{
			var products = await _productAccessor.GetAll();
			var mapped = products.Select(x => _mapper.Map<ProductDto>(x)).ToList();
			return ResultDto<QuoteDto>.Ok<IReadOnlyCollection<ProductDto>>(mapped);
		}
	}
}