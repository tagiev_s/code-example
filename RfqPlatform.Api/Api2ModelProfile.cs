﻿using AutoMapper;
using RfqPlatform.Abstracts.Commands;
using RfqPlatform.Api.Dtos.Commands;

namespace RfqPlatform.Api
{
	public class Api2ModelProfile : Profile
	{
		public Api2ModelProfile()
		{
			CreateMap<QuoteRequestCommandDto, QuoteRequestCommand>();
			CreateMap<TradeCreateCommandDto, TradeCreateCommand>();
		}
	}
}