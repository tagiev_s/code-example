﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RfqPlatform.Api.SwaggerFilter
{
	public class CustomResponseNameFilter : IOperationFilter
	{
		public void Apply(OpenApiOperation operation, OperationFilterContext context)
		{
			var name = context.MethodInfo.GetCustomAttribute<CustomResponseNameAttribute>()?.Name;
			if (name != null)
			{
				var content = operation.Responses.FirstOrDefault().Value.Content;
				var type = content.Values.FirstOrDefault();
				var refer = type.Schema.Reference;

				if (context.SchemaRepository.Schemas.TryGetValue(refer.Id, out var typeParamSchema))
				{
					context.SchemaRepository.Schemas.Remove(refer.Id);
					context.SchemaRepository.Schemas.Add(name, typeParamSchema);
					refer.Id = name;
				}
			}
			
		}
	}
}
