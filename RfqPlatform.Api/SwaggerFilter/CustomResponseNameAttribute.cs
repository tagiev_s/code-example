﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.SwaggerFilter
{
	[System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = false)]
	public class CustomResponseNameAttribute : Attribute
	{
		public CustomResponseNameAttribute(string name)
		{
			Name = name ?? throw new ArgumentNullException(nameof(name));
		}

		public string Name { get; set; }
	}
}
