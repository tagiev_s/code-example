using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Reflection;
using RfqPlatform.Api.SwaggerFilter;
using System.IO;
using System.Net.Http;
using Infrastructure;
using Interfaces.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Pricer.Client;
using RfqPlatform.DL;
using RfqPlatform.Core;
using ClientConfig.Client;
using Microsoft.Extensions.Caching.Memory;
using Infrastructure.Strategies;
using Infrastructure.Middleware;

namespace RfqPlatform.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers().AddJsonOptions(options =>
			{
				options.JsonSerializerOptions.IgnoreNullValues = true;
				options.JsonSerializerOptions.Converters.Add(new System.Text.Json.Serialization.JsonStringEnumConverter());
			});
			services.AddSwaggerGen(c =>
			{
				c.CustomSchemaIds(x => x.GetCustomAttributes<DisplayNameAttribute>().SingleOrDefault()?.DisplayName ?? (!x.IsGenericType ? x.Name : x.FullName));
				c.OperationFilter<CustomResponseNameFilter>();
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "RFQ API", Version = "v1" });

				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);
				c.EnableAnnotations();
				c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
				{
					In = ParameterLocation.Header,
					Type = SecuritySchemeType.OAuth2,
					Flows = new OpenApiOAuthFlows
					{
						Password = new OpenApiOAuthFlow
						{
							TokenUrl = new Uri(""),
							Scopes = new Dictionary<string, string>
							{
							}
						}
					}
				});
				c.AddSecurityRequirement(new OpenApiSecurityRequirement {
					{
						new OpenApiSecurityScheme
						{
							Reference = new OpenApiReference
							{
								Type = ReferenceType.SecurityScheme,
								Id = "oauth2"
							}
						},
						new string[] { }
					}
				});
			});

			services.AddAutomapper();

			services.AddTransient(sp =>
			{
				var token = sp.GetRequiredService<IJwtTokenProvider>().GetToken();
				var correlationId = sp.GetRequiredService<ICorrelationIdGeneratorStrategy>().GenerateCorrelationId();
				return new PricerClient(Configuration["Endpoints:Pricer"], new HttpClient(), token, correlationId);
			});

			services.AddTransient<PricerService, PricerService>();

			services.AddTransient(sp =>
			{
				var token = sp.GetRequiredService<IJwtTokenProvider>().GetToken();
				var correlationId = sp.GetRequiredService<ICorrelationIdGeneratorStrategy>().GenerateCorrelationId();
				return new ClientConfigClient(Configuration["Endpoints:ClientConfig"], new HttpClient(), token, correlationId);
			});

			services.AddDal(Configuration);
			services.AddCore(Configuration);

			services.AddInfrastructureServices();

			services.AddHttpContextAccessor();

			services.ClientConfigClienConfigureDependencies();
			services.PricerClienConfigureDependencies();

			services.AddAuthentication(x =>
				{
					x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
					x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
				})
				.AddJwtBearer(x =>
				{
					x.Authority = Configuration["Auth:Authority"];
					x.Audience = Configuration["Auth:Audience"];
					x.RequireHttpsMetadata = false;
					x.SaveToken = true;
					x.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuerSigningKey = false,
						ValidateAudience = true
					};

					x.Events = new JwtBearerEvents
					{
						OnAuthenticationFailed = c =>
						{
							c.NoResult();

							c.Response.StatusCode = 500;
							c.Response.ContentType = "text/plain";

							return c.Response.WriteAsync(c.Exception.ToString());

						}
					};
				});
			services.AddSingleton<IMemoryCache, MemoryCache>();
			services.AddTransient<ICorrelationIdGeneratorStrategy, CorrelationIdOnceNewStrategy>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "RFQ API V1");
				c.OAuthClientId("rfq-localhost");
				c.OAuthRealm("RFQ");
				//c.url
			});

			app.UseRouting();

			app.UseAuthentication();

			app.UseMiddleware<LoggingMiddleware>();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
