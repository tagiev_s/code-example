﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RfqPlatform.Api.Helpers
{
	public class ErrorCodes
	{
		public static string ValidationError = "error.validation";
		public static string UnexpectedError = "error.unexpected";
	}
}
