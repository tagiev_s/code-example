﻿using System;

namespace Pricer.Abstracts.Models
{
    public class ProductQuote
    {
        public ProductQuote(int clientId, int productId, DateTime quoteTime, decimal bid, decimal ask)
        {
            ClientId = clientId;
            ProductId = productId;
            QuoteTime = quoteTime;
            Bid = bid;
            Ask = ask;
        }

        public int ClientId { get; }
        public int ProductId { get; }
        public DateTime QuoteTime { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
    }
}
