﻿namespace Pricer.Abstracts.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string BaseCurrency { get; set; }
        public string ValuationCurrency { get; set; }
    }
}