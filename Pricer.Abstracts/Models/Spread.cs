﻿namespace Pricer.Abstracts.Models
{
    public class Spread
    {
        public decimal SpreadValue { get; set; }
        public string SpreadClass { get; set; }
        public string SpreadType { get; set; }
    }
}