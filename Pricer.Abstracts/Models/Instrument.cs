﻿namespace Pricer.Abstracts.Models
{
    public class Instrument
    {
        public string Code { get; set; }

        public bool IsStrong { get; set; }
    }
}