﻿using System;

namespace Pricer.Abstracts.Models
{
    public class Quote
    {
        public Quote(string instrument, DateTime quoteTime, decimal bid, decimal ask)
        {
            Instrument = instrument;
            QuoteTime = quoteTime;
            Bid = bid;
            Ask = ask;
        }
        public string Instrument { get; set; } 
        public DateTime QuoteTime { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
    }
}