﻿using System.Collections.Generic;

namespace Pricer.Abstracts.Models
{
    public class ProductCard
    { 
        public List<Spread> Spreads { get; set; } = new List<Spread>();

        public int ClientId { get; set; }

        public Product Product { get; set; } = new Product();
    }
}