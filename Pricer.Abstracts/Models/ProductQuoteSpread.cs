﻿using System;

namespace Pricer.Abstracts.Models
{
    public class ProductQuoteSpread
    {
        public ProductQuote ProductQuote { get; set; }
        public Spread Spread { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }

        public ProductQuoteSpread(ProductQuote productQuote, Spread spread, decimal bid, decimal ask)
        {
            ProductQuote = productQuote;
            Spread = spread;
            Bid = bid;
            Ask = ask;
        }
        
    }
}