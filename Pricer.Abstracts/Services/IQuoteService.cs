﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Pricer.Abstracts.Models;

namespace Pricer.Abstracts.Services
{
    public interface IQuoteService
    {
        Task<ProductQuote?> GetQuote(int productId, int clientId);
        Task<ProductQuoteSpread[]> GetQuotesWithSpreads();
    }
}
