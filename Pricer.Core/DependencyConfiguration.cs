﻿using System.Net.Http;
using ClientConfig.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pricer.Abstracts.Services;
using Pricer.Core.Services;

namespace Pricer.Core
{
    public static class DependencyConfiguration
    {
        public static void ConfigureDependencies(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddTransient<IQuoteService, QuoteService>();
        } 
    }
}
