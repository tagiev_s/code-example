using Infrastructure.Strategies;
using Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Exception = System.Exception;

namespace Infrastructure.Middleware
{
	public class LoggingMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly ILogger _logger;
		private readonly ICorrelationIdProvider _correlationIdProvider;

		public LoggingMiddleware(RequestDelegate next,
			ILogger<LoggingMiddleware> logger,
			ICorrelationIdProvider correlationIdProvider)
		{
			_next = next;
			_logger = logger;
			_correlationIdProvider = correlationIdProvider;
		}

		public async Task Invoke(HttpContext context)
		{

			context.Request.EnableBuffering();

			using StreamReader streamReader = new StreamReader(context.Request.Body);

			var request = await streamReader.ReadToEndAsync();

			context.Request.Body.Position = 0;

			var attributes = new Dictionary<string, object>
			{
				{"Request", request},
				{"Method", context.Request.Method},
				{"Url", context.Request.Path.Value},
				{"QueryString", context.Request.QueryString},
				{"CorrelationId", _correlationIdProvider.GetCorrelationId()}
			};

			var userName = context.User.Claims.FirstOrDefault(x => x.Type == "name");

			if (userName != null)
				attributes["User"] = userName.Value;

			try
			{
				await _next(context);
			}
			catch (Exception ex)
			{
				attributes["Exception"] = ex.ToString();
				throw;
			}
			finally
			{
				var template = string.Join(" ", attributes.Select(x => $"{x.Key} {{{x.Key}}}"));
				var values = attributes.Values.Select(x => x).ToArray();

				if (attributes.ContainsKey("Exception"))
					_logger.LogError(template, values);
				else
					_logger.LogInformation(template, values);
			}
		}

	}
}