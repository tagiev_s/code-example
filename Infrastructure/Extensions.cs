﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Services;
using Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class Extensions
    {
        public static void AddInfrastructureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IJwtTokenProvider, JwtTokenProvider>();
            serviceCollection.AddTransient< ICorrelationIdProvider, CorrelationIdProvider>();
        }
    }
}
