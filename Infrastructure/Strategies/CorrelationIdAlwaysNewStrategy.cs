﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Strategies
{
	public class CorrelationIdAlwaysNewStrategy : ICorrelationIdGeneratorStrategy
	{
		public string GenerateCorrelationId()
		{
			return Guid.NewGuid().ToString();
		}
	}
}
