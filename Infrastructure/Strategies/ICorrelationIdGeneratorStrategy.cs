﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Strategies
{
	public interface ICorrelationIdGeneratorStrategy
	{
		string GenerateCorrelationId();
	}
}
