﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Strategies
{
	public class CorrelationIdOnceNewStrategy : ICorrelationIdGeneratorStrategy
	{
		private readonly string correlationId = Guid.NewGuid().ToString();
		public string GenerateCorrelationId()
		{
			return correlationId;
		}
	}
}
