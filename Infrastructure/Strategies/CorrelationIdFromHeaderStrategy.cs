﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Strategies
{
	public class CorrelationIdFromHeaderStrategy: ICorrelationIdGeneratorStrategy
	{
		private readonly IHttpContextAccessor _httpContextAccessor;

		public CorrelationIdFromHeaderStrategy(IHttpContextAccessor httpContextAccessor)
		{
			_httpContextAccessor = httpContextAccessor;
		}

		public string GenerateCorrelationId()
		{
			string correlationId = _httpContextAccessor.HttpContext.Request.Headers["CorrelationId"];

			return correlationId;
		}
	}
}
