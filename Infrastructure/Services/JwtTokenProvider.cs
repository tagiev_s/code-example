﻿using System;
using Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Services
{
    public class JwtTokenProvider : IJwtTokenProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public JwtTokenProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetToken()
        {
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];

            if(!token.StartsWith("Bearer"))
                throw new Exception($"Invalid token {token}");

            return token.Split(" ")[1];
        }
    }
}